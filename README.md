# Auth App

## Introduction

This is a basic Django and Vue.js project that shows an example how to integrate Django and Vue.js CLI app. It also tries to show an example on one of the best practice that I have found (don't remember exact source) on how JWT should be used.

## How JWT is used

In this project, JWT is used in the following way.

* When user logins,  the API sends refresh token as an HTTP only cookie, and access token as a normal JSON response.
* UI, then stores the access token in memory(Vuex) and is used to authorize the user.
* The refresh token is only send back to server to refresh access token
* Whenever user refresh the page or visits later, the page tries to gain new access token using refresh token from the HTTP only cookie.
* When the user logouts, a request is sent to the API, which then deletes the refresh token.

This way the access token or refresh token is not stored in local storage which most of the app uses and is a bad practice. As for refresh token, since it is stored using HTTP only cookie, it is mostly safe. There are still chance of some attacks like CSRF, but it is mostly secure as refresh token is only sent while refreshing access token and not with every request.

## How Django & Vue.js is integrated

This project uses the idea that was popularized by [@Ejez](https://github.com/ejez) and uses example from [@EugeneDae](https://github.com/EugeneDae/django-vue-cli-webpack-demo).

One caveat of that example is that if user tries to access a URL from Django e.g. '/admin' (Django's admin), the site then redirect to Vue.js page and returns 404 error. This is happening as Django's default pattern has '/' at the end and the user has entered '/admin' and not '/admin/'. To solve this, index.html of Vue.js needs to be server using re_path, which ignores any Django URL without '/' at the end. If you have many endpoints at Django then this is a problem.

This is solved by generating a regex of all the URLs defined in Django, which is then used to serve index.html by ignoring those URLs.

The generator does have a known issue. If you use a re_path in Django's URL, it will not work properly. I have set an example for static and media URLs which uses re_path/regex, to just use '/static' and '/media' instead of regex.

## Project Setup

* Read the README form api & ui folder
* Run both api & ui
