import { JwtStore } from '@/store/modules'

export async function WaitUntilRefreshed() {
  while (JwtStore.RefreshingToken) {
    await new Promise(resolve => setTimeout(resolve, 100))
  }
}

export function HasJwtExpired() {
  const exp = JwtStore.DecodedPayload.exp
  const now = Math.floor(new Date().getTime() / 1000)
  if (exp <= now) {
    return true
  }
  return false
}
