import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import './quasar'
import { LoadingBar } from 'quasar'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

LoadingBar.setDefaults({
  color: 'primary',
  size: '5px',
  position: 'bottom'
})
