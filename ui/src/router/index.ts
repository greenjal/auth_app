import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    component: () =>
      import(
        /* webpackChunkName: "general"  */ '@/views/layouts/MainLayout.vue'
      ),
    children: [
      {
        path: '/',
        name: 'Home',
        component: () =>
          import(/* webpackChunkName: "general"  */ '@/views/pages/Home.vue')
      },
      {
        path: '/about',
        name: 'About',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "general" */ '@/views/pages/About.vue')
      },
      {
        path: '/login',
        name: 'Login',
        component: () =>
          import(/* webpackChunkName: "general" */ '@/views/pages/Login.vue')
      },
      {
        path: '/profile',
        name: 'Profile',
        component: () =>
          import(/* webpackChunkName: "general" */ '@/views/pages/Profile.vue')
      }
    ]
  },
  {
    path: '*',
    name: 'NotFound',
    component: () =>
      import(/* webpackChunkName: "general" */ '@/views/pages/NotFound.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  // base: process.env.BASE_URL,
  routes
})

export default router
