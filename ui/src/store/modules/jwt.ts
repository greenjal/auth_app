import store from '@/store'
import {
  Module,
  VuexModule,
  getModule,
  MutationAction,
  Mutation
} from 'vuex-module-decorators'
import jwtDecode from 'jwt-decode'

import { UserLoginInfo, JWT, DecodedJWTPayload } from '@/store/models/jwt'
import API from '@/store/api/jwt'
import router from '@/router'

@Module({ name: 'JwtStore', dynamic: true, store })
class JwtStore extends VuexModule {
  AccessToken = ''
  DecodedPayload: DecodedJWTPayload = {} as DecodedJWTPayload
  isLoggedIn = false
  RefreshingToken = true

  @Mutation
  refreshingToken() {
    this.RefreshingToken = true
  }

  get loggedIn() {
    return this.isLoggedIn
  }

  @MutationAction
  async getJWT(user: UserLoginInfo) {
    const detail = (await API.getJWT(user)) as JWT
    const decodedPayload = jwtDecode(detail.access)
    return {
      AccessToken: detail.access,
      DecodedPayload: decodedPayload,
      isLoggedIn: true
    }
  }

  @MutationAction
  async refreshAccessToken() {
    let detail = (await API.refreshAccessToken()) as JWT

    // eslint-disable-next-line
    let decodedPayload = {} as any
    let isLoggedIn = false
    if (!detail || !detail.access) {
      const loggedIn = (this.state as JwtStore).isLoggedIn
      detail = {} as JWT
      detail.access = ''
      decodedPayload = {}
      if (loggedIn) {
        console.log('You have been logged out')
        router.push({ name: 'Home' })
      }
    } else {
      decodedPayload = jwtDecode(detail.access)
      isLoggedIn = true
    }
    return {
      AccessToken: detail.access,
      DecodedPayload: decodedPayload,
      isLoggedIn: isLoggedIn,
      RefreshingToken: false
    }
  }

  @MutationAction
  async clearJWT() {
    await API.clearJWT()
    return {
      AccessToken: '',
      DecodedPayload: {},
      isLoggedIn: false
    }
  }
}

export default getModule(JwtStore)
