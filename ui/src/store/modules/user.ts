import store from '@/store'
import {
  Module,
  VuexModule,
  getModule,
  MutationAction
} from 'vuex-module-decorators'
import { UserDetail } from '@/store/models/user'
import API from '@/store/api/user'

@Module({ name: 'UserStore', dynamic: true, store })
class UserStore extends VuexModule {
  UserDetail: UserDetail = {} as UserDetail
  @MutationAction
  async getUserDetail() {
    const userDetail = (await API.getUserDetail()) as UserDetail
    return { UserDetail: userDetail }
  }
}

export default getModule(UserStore)
