import { getApi } from '@/store/api'
import { Action } from '@/store/api/actions'

export default new (class UserAPI {
  public async getUserDetail() {
    const response = await getApi(Action.UserDetail)
    return response
  }
})()
