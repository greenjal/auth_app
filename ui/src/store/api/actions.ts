export enum Action {
  API = '/api',
  Token = 'token',
  RefreshToken = 'token/refresh',
  ClearToken = 'token/clear',
  UserDetail = 'user_detail'
}
