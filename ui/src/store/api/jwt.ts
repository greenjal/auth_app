import { postAPI } from '@/store/api'
import { UserLoginInfo } from '@/store/models/jwt'
import { Action } from '@/store/api/actions'

export default new (class JWTAPI {
  public async getJWT(user: UserLoginInfo) {
    const response = await postAPI(Action.Token, user, {
      withCredentials: true
    })
    return response
  }

  public async refreshAccessToken() {
    try {
      const response = await postAPI(
        Action.RefreshToken,
        {},
        {
          withCredentials: true
        }
      )
      return response
    } catch (error) {
      console.error('Error: ', error)
    }
  }

  public async clearJWT() {
    const response = await postAPI(
      Action.ClearToken,
      {},
      {
        withCredentials: true
      }
    )
    return response
  }
})()
