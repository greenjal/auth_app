export interface UserDetail {
  id: string
  username: string
  first_name: string
  last_name: string
  email: string
  date_joined: string
  last_login: string
}
