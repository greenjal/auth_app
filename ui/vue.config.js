module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? process.env.VUE_APP_USE_S3 && process.env.VUE_APP_S3_URL
        ? process.env.VUE_APP_S3_URL
        : '/'
      : 'http://127.0.0.1:8080',
  assetsDir: 'static',
  outputDir: '../api/frontend',
  indexPath: '../templates/base-vue.html', // relative to outputDir!
  productionSourceMap: false,
  filenameHashing: false,
  runtimeCompiler: true,
  pwa: {
    name: 'Auth App',
    shortName: 'AA',
    manifestPath: 'static/manifest.json',
    iconPaths: {
      favicon32: 'static/img/icons/favicon-32x32.png',
      favicon16: 'static/img/icons/favicon-16x16.png',
      appleTouchIcon: 'static/img/icons/apple-touch-icon-152x152.png',
      maskIcon: 'static/img/icons/safari-pinned-tab.svg',
      msTileImage: 'static/img/icons/msapplication-icon-144x144.png'
    },
    workboxOptions: {
      swDest: 'static/service-worker.js',
      precacheManifestFilename: 'static/precache-manifest.[manifestHash].js'
    }
  },

  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false
    }
  },
  transpileDependencies: ['quasar'],
  devServer: {
    proxy: {
      '/api*': {
        // Forward frontend dev server request for /api to django dev server
        target: 'http://localhost:8000/'
      },
      '/admin*': {
        // Forward frontend dev server request for /api to django dev server
        target: 'http://localhost:8000/'
      }
    },
    public: 'http://127.0.0.1:8080',
    hotOnly: true,
    headers: { 'Access-Control-Allow-Origin': '*' },
    writeToDisk: filePath => filePath.endsWith('index.html')
  },
}
