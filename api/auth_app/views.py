from django.contrib.auth import get_user_model
from django.conf import settings
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.viewsets import GenericViewSet
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from auth_app.serializers import EmptySerializer, TokenObtainPairSerializer, UserSerializer


def refresh_cookie(response):
    args = ['refresh', response.data['refresh']]
    kwargs = {'httponly': True}
    response.set_cookie(*args, **kwargs)
    return response


class TokenObtainPairViewSet(GenericViewSet, TokenObtainPairView):
    """
        Custom TokenObtainPairViewSet class that will send refresh token as an http only cookie.
    """
    serializer_class = TokenObtainPairSerializer

    def create(self, request, *args, **kwargs):
        response = super().post(request=request, *args, **kwargs)
        response = refresh_cookie(response)
        # Uncomment the line below if you don't want to send refresh token as response data
        # response.data.pop('refresh')
        return response


class TokenRefreshViewSet(GenericViewSet, TokenRefreshView):
    """
        Custom TokenRefreshViewSet class that takes refresh token from cookies when refresh is not present in post data.
        It also returns refresh token if ROTATE_REFRESH_TOKENS is true and it gets 'refresh' in response's data.
    """

    def create(self, request, *args, **kwargs):
        if not 'refresh' in request.data.keys() and 'refresh' in request.COOKIES.keys():
            request.data['refresh'] = request.COOKIES['refresh']
        try:
            response = super().post(request=request, *args, **kwargs)
            if 'refresh' in response.data.keys():
                response = refresh_cookie(response)
                # , samesite="none", secure=settings.SECURE_COOKIES)
                # Uncomment the line below if you don't want to send refresh token as response data
                # response.data.pop('refresh')
        except Exception as e:
            response = Response(status=HTTP_401_UNAUTHORIZED)
            response.delete_cookie('refresh')

        return response


class ClearTokenViewSet(GenericViewSet):
    """
        Create ViewSet to clear refresh token in client.
    """
    serializer_class = EmptySerializer

    def create(self, request, *args, **kwargs):
        response = Response()
        response.delete_cookie('refresh')
        return response


class UserDetailViewSet(GenericViewSet):
    """
        ListViewSet that will return the currently authenticated user
    """
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()

    def list(self, request, *args, **kwargs):
        serializer = self.serializer_class(request.user)
        # import time
        # time.sleep(1)
        return Response(serializer.data)
