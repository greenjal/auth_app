from django.urls import path
# from rest_framework_simplejwt.views import TokenRefreshView
from rest_framework.routers import DefaultRouter
from .views import TokenObtainPairViewSet, UserDetailViewSet, TokenRefreshViewSet, ClearTokenViewSet

router = DefaultRouter()

router.register('user_detail', UserDetailViewSet, basename='user_detail')
router.register('token', TokenObtainPairViewSet, basename='token')
router.register('token/refresh', TokenRefreshViewSet, basename='token_refresh')
router.register('token/clear', ClearTokenViewSet, basename='token_clear')

urlpatterns = [
    # path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    # path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

urlpatterns += router.urls
