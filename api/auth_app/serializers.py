from django.contrib.auth import get_user_model
from django.db.models import fields
from rest_framework.serializers import ModelSerializer, Serializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class TokenObtainPairSerializer(TokenObtainPairSerializer):
    """
        Custom TokenObtainPairSerializer class that will add username to the token payload
        This can then be decoded by client to use the available data
    """
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['username'] = user.username
        return token


class UserSerializer(ModelSerializer):
    """
        Custom TokenObtainPairSerializer class that will add username to the token payload
        This can then be decoded by client to use the available data
    """
    class Meta:
        model = get_user_model()
        fields = ("id", 'username', 'first_name', 'last_name',
                  'email', 'date_joined', 'last_login')


class EmptySerializer(Serializer):
    """
        Empty Seralizer that does not take or return any value
    """
    pass
