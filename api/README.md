# Auth App API

## Project Setup

1. Setup pipenv if you dont have it. You can learn about pipenv and how to setup from [here](https://docs.pipenv.org/ "Pipenv: Python Dev Workflow for Humans").
   Additionally you can also setup pyenv from [here](https://github.com/pyenv/pyenv) if you want to switch to the specified python version.

2. Create virtual environment and sync dependencies

   ```terminal/cmd
   pipenv sync
   ```

3. Add .env file in the root folder. You can make a copy of .env.sample and modify it.

4. Setup your database and then run migrations for database.

   ```terminal/cmd
   pipenv run manage migrate

   ```

5. Run the development server.

   ```terminal/cmd
   pipenv run server
   ```

You can also activate pipenv shell and run step 4 and 5 as follows:

```terminal/cmd
pipenv shell
python manage.py migrate
python manage.py runserver

```
