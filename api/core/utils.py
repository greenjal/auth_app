from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.

    print(response.data)
    if isinstance(exc.detail, list):
        data = {}
        data['errors'] = response.data
        response.data = data
        response.data['message'] = exc.detail[0]
    elif isinstance(exc.detail, dict):
        data = {}
        data['errors'] = response.data
        response.data = data
        response.data['message'] = f"{list(exc.detail.keys())[0]}: {list(exc.detail.values())[0][0]}"
    else:
        response.data['message'] = exc.detail

    return response
