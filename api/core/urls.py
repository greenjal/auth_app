"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from core import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('auth_app.urls')),
    path('api-auth/', include('rest_framework.urls')),
    re_path('robots.txt', views.robots, name="robots"),
    re_path('favicon.ico', views.favicon, name="favicon"),
]

"""
Generates a regex with reverse lookup of urls including static an>
This regex is then used in re_path of Vue's index.html.
This is done to handle any django url without a trailing slash.
"""
urls = list()
if settings.STATIC_URL:
    static_url = settings.STATIC_URL
    if static_url != '/':
        static_url = static_url.strip('/')

    urls.append(static_url)
if settings.MEDIA_URL:
    media_url = settings.MEDIA_URL
    if media_url != '/':
        media_url = media_url.strip('/')
    urls.append(media_url)
for url in urlpatterns:
    urls.append(str(url.pattern).strip('/'))
pattern_base = f'(?!{"|".join(urls)})'
pattern_index = f'^{pattern_base}.*$'
urlpatterns += [
    re_path(pattern_index, views.index_view, name="Home"),
]
